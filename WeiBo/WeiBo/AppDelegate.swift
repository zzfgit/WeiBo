//
//  AppDelegate.swift
//  WeiBo
//
//  Created by 左忠飞 on 2016/12/11.
//  Copyright © 2016年 左忠飞. All rights reserved.
//

import UIKit
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        //检测设备版本
        if #available(iOS 10.0, *){
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.sound,.carPlay]) { (success, error) in
            print("授权" + (success ? "成功":"失败"))
            }
        }else{
            //10.0以下的方法
            //申请用户授权显示通知(提示条,声音,角标数量)
            let notifySettings = UIUserNotificationSettings(types: [.alert,.badge,.sound], categories: nil)
            application.registerUserNotificationSettings(notifySettings)
        }
        
        
        
        window = UIWindow()
        window?.backgroundColor = UIColor.white
        window?.rootViewController = ZFMainViewController()
        window?.makeKeyAndVisible()
        
        laodAppInfo()
        
        return true
    }
}



//  MARK: - 从服务器加载应用程序的信息
extension AppDelegate {
    fileprivate func laodAppInfo() {
        
        //1 模拟异步
        DispatchQueue.global().async {
            //1 url 
            let url = Bundle.main.url(forResource: "main.json", withExtension: nil)
            //2 data
            let data = NSData(contentsOf: url!)
            
            //3 写入磁盘
            let docDir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask,true)[0]
            
            let jsonPath = (docDir as NSString).appendingPathComponent("main.json")
            
            data?.write(toFile: jsonPath, atomically: true)
            
            print("应用程序加载完毕\(jsonPath)")
            
            
        }
    }
}














