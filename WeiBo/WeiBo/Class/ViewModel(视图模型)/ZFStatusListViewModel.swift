//
//  ZFStatusListViewModel.swift
//  WeiBo
//
//  Created by 左忠飞 on 2016/12/25.
//  Copyright © 2016年 左忠飞. All rights reserved.
//

import Foundation

///微博数据列表视图模型

//父类的选择
//如果累需要使用 kvc 或者字典转模型框架设置对象值,类就需要继承自 NSobject
//如果类只是包装一些代码逻辑,(谢了一些函数)可以不用继承任何父类,更加轻量级
//如果用 oc 写,一律都继承自 NSobject

//上拉刷新最大尝试次数
fileprivate let maxPullupTryTimes = 3

//负责微博的数据处理
class ZFStatusListViewModel {
    
    //微博模型数组懒加载
    lazy var statusList = [ZFStatus]()
    
    //上啦刷新错误次数
    fileprivate var pullupErrorTimes = 0
    
    
    /// 加载微博列表
    ///
    /// - Parameters:
    ///   - pullup: 是否上啦刷新
    ///   - completion: 回调,网络请求是否成功
    func loadStatus(pullup:Bool, completion:@escaping (_ isSuccess:Bool,_ shouldRefresh:Bool)->()) {
        
        
        //判断是否是上啦刷新,同时检查刷新多屋
        if pullup && pullupErrorTimes > maxPullupTryTimes {
            completion(true,false)
            return
        }
        
        //取出数组中的第一条微博的 id
        let since_id = pullup ? 0 : (statusList.first?.id ?? 0)
        //取出数组的最后一条微博的 id
        let max_id = !pullup ? 0 : (statusList.last?.id ?? 0)
        
        ZFNetworkManager.shared.statusList (since_id: since_id,max_id: max_id){ (list, isSuccess) in
            
            //1 字典转模型
            guard let array = NSArray.yy_modelArray(with: ZFStatus.self, json: list ?? []) as? [ZFStatus] else{
                completion(isSuccess,false)
                return
            }
            
            print("刷新到\(array.count)条数据")
            
            //2 拼接数据
            //下拉刷新,将结果拼接在数组的前面
            if pullup {
                self.statusList += array
            }else{
                self.statusList = array+self.statusList
            }
            
            //判断上啦刷新的数据量
            if pullup && array.count == 0 {
                
                self.pullupErrorTimes += 1
                
                print("*********",self.pullupErrorTimes)

                
                completion(isSuccess,false)
            }else{
                //3 完成回调
                completion(isSuccess,true)
            }
            
            
            
        }
    }
    
}








