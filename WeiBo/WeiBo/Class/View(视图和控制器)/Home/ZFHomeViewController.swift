//
//  ZFHomeViewController.swift
//  WeiBo
//
//  Created by 左忠飞 on 2016/12/11.
//  Copyright © 2016年 左忠飞. All rights reserved.
//

import UIKit

//定义全局常量,尽量使用 private 修饰,否则到处都可以访问
fileprivate let cellId = "cellId"

class ZFHomeViewController: ZFBaseViewController {

//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//    }
    
//    //微博数据数组
//    fileprivate lazy var statusList = [String]()
    
    //列表视图模型
    fileprivate lazy var listViewModel = ZFStatusListViewModel()
    
    
    
    override func loadData() {
        
        print("准备加载数据\(self.listViewModel.statusList.last?.text)")
        
        listViewModel.loadStatus(pullup: self.isPullup) { (isSuccess,hasMorePullup) in
            print("数据刷新完毕")
            
            //回复上啦刷新标记
            self.isPullup = false
            
            //结束刷新控件
            self.refreshControl?.endRefreshing()
            //刷新表格
            if hasMorePullup {
                
                self.tableView?.reloadData()
                
            }
        }
        
        
    }
    
    @objc fileprivate func showFriends(){
        print(#function)
        
        let vc = ZFDemoViewController()
        
        vc.hidesBottomBarWhenPushed = true
        
        //push 的动作是 nav 做的
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
//  MARK: - 表格数据源方法
extension ZFHomeViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listViewModel.statusList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //1 取出 cell
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        //2 设置 cell
        cell.textLabel?.text = listViewModel.statusList[indexPath.row].text
        
        //3 返回 cell
        return cell
    }
    
}

//  MARK: - 设置界面
extension ZFHomeViewController {
    //重写父类的方法
    override func setupTabelView() {
        super.setupTabelView()

        //使用封装的便利构造函数设置BarButtonItem 
        navItem.leftBarButtonItem = UIBarButtonItem.init(title: "好友", fontSize: 16, target: self, action: #selector(showFriends))
        
        //注册原型 cell
        tableView?.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        
        setupNavTitle()
    }
    
    //设置导航栏标题
    fileprivate func setupNavTitle() {
        
        let userName = ZFNetworkManager.shared.userAccount.screen_name
        
        let button = ZFTitleButton(title: userName)
        
        navItem.titleView = button
        
        button.addTarget(self, action: #selector(clickTitleButton(btn:)), for: .touchUpInside)
    }
    
    @objc fileprivate func clickTitleButton(btn:UIButton) {
        //设置选中状态
        btn.isSelected = !btn.isSelected
    }
    
    
}








