//
//  ZFVisitorView.swift
//  WeiBo
//
//  Created by 左忠飞 on 2016/12/20.
//  Copyright © 2016年 左忠飞. All rights reserved.
//

import UIKit

class ZFVisitorView: UIView {

    
    //注册按钮
    lazy var registerButton:UIButton = UIButton.cz_textButton(
        "注册",
        fontSize: 16,
        normalColor: UIColor.orange,
        highlightedColor: UIColor.black,
        backgroundImageName:"common_button_white_disable")
    
    //登录按钮
    lazy var loginButton:UIButton = UIButton.cz_textButton(
        "登录",
        fontSize: 16,
        normalColor: UIColor.darkGray,
        highlightedColor: UIColor.black,
        backgroundImageName:"common_button_white_disable")
    

    
    ///访客视图的信息字典[imageName:message]
    ///如果是首页 imageName = ""
    var visitorInfo:[String:String]? {
        didSet {
            //1 取字典信息
            guard let imageName = visitorInfo?["imageName"],
                let message = visitorInfo?["message"] else{
                    return
            }
            //2 设置消息
            tipLabel.text = message
            //3 设置图像,首页不需要设置
            if imageName == "" {
                stattAnimation()
                return
            }
            iconView.image = UIImage(named: imageName)
            
            houseView.isHidden = true
            maskIconView.isHidden = true
        }
    }
    
    
    //  MARK: - 构造函数
    override init(frame:CGRect){
        super.init(frame: frame)
        
        setupUI()
    }
    
    //  MARK: - 旋转图标动画
    fileprivate func stattAnimation(){
        let anim = CABasicAnimation(keyPath: "transform.rotation")
        
        anim.toValue = 2 * M_PI
        anim.repeatCount = MAXFLOAT
        anim.duration = 5
        
        //动画完成不删除,如果iconView 被释放,动画会一起被销毁
        //设置连续播放的动画非常有用
        anim.isRemovedOnCompletion = false
        
        
        //将动画添加到图层
        iconView.layer.add(anim, forKey: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    //  MARK: - 私有控件
    //图像视图
    fileprivate lazy var iconView: UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_image_smallicon"))
    
    //图像遮罩视图
    fileprivate lazy var maskIconView :UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_mask_smallicon"))
    
    //小房子
    fileprivate lazy var houseView:UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_image_house"))
    
    //提示标签
    //懒加载属性只有调用 UIkit 控件制定的构造函数不需要制定类型,其他的都需要制定类型
    fileprivate lazy var tipLabel:UILabel = UILabel.cz_label(
        withText: "关注一些人,回这里看看有什么惊喜,回这里看看有什么惊喜",
        fontSize: 14,
        color: UIColor.darkGray)
    
    
    

}

//  MARK: - 设置界面
extension ZFVisitorView {
    func setupUI() {
        //在开发的时候,如果能用颜色,就不要用图像,颜色的效率更高,节省性能
        backgroundColor = UIColor.cz_color(withHex: 0xEDEDED)
        
        //1 添加控件
        addSubview(iconView)
        addSubview(maskIconView)
        addSubview(houseView)
        addSubview(tipLabel)
        addSubview(registerButton)
        addSubview(loginButton)
        
        // 文本居中
        tipLabel.textAlignment = .center
        
        //2 取消 autoresizing
        for v in subviews {
            v.translatesAutoresizingMaskIntoConstraints = false
        }
        //3 自动布局
        let margin:CGFloat = 25
        
        // 1>转轮视图
        addConstraint(NSLayoutConstraint(
            item: iconView,
            attribute: .centerX,
            relatedBy: .equal,
            toItem: self,
            attribute: .centerX,
            multiplier: 1.0,
            constant: 0))
        addConstraint(NSLayoutConstraint(
            item: iconView,
            attribute:
            .centerY,
            relatedBy: .equal,
            toItem: self,
            attribute: .centerY,
            multiplier: 1.0,
            constant: -60))
        
        // 2>小房子
        addConstraint(NSLayoutConstraint(
            item: houseView,
            attribute: .centerX,
            relatedBy: .equal,
            toItem: iconView,
            attribute: .centerX,
            multiplier: 1.0,
            constant: 0))
        addConstraint(NSLayoutConstraint(
            item: houseView,
            attribute:
            .centerY,
            relatedBy: .equal,
            toItem: iconView,
            attribute: .centerY,
            multiplier: 1.0,
            constant: 0))
        
        //3 提示标签
        addConstraint(NSLayoutConstraint(
                item: tipLabel,
                attribute: .centerX,
                relatedBy: .equal,
                toItem: iconView,
                attribute: .centerX,
                multiplier: 1.0,
                constant: 0))
        addConstraint(NSLayoutConstraint(
            item: tipLabel,
            attribute: .bottom,
            relatedBy: .equal,
            toItem: iconView,
            attribute: .bottom,
            multiplier: 1.0,
            constant: margin))
        addConstraint(NSLayoutConstraint(
            item: tipLabel,
            attribute: .width,
            relatedBy: .equal,
            toItem: nil,
            attribute: .notAnAttribute,
            multiplier: 1.0,
            constant: 236))
        
        //4 注册按钮
        addConstraint(NSLayoutConstraint(
            item: registerButton,
            attribute: .left,
            relatedBy: .equal,
            toItem: tipLabel,
            attribute: .left,
            multiplier: 1.0,
            constant: 0))
        addConstraint(NSLayoutConstraint(
            item: registerButton,
            attribute: .top,
            relatedBy: .equal,
            toItem: tipLabel,
            attribute: .bottom,
            multiplier: 1.0,
            constant: margin))
        addConstraint(NSLayoutConstraint(
            item: registerButton,
            attribute: .width,
            relatedBy: .equal,
            toItem: nil,
            attribute: .notAnAttribute,
            multiplier: 1.0,
            constant: 100))
        //5 登录按钮
        addConstraint(NSLayoutConstraint(
            item: loginButton,
            attribute: .right,
            relatedBy: .equal,
            toItem: tipLabel,
            attribute: .right,
            multiplier: 1.0,
            constant: 0))
        addConstraint(NSLayoutConstraint(
            item: loginButton,
            attribute: .top,
            relatedBy: .equal,
            toItem: tipLabel,
            attribute: .bottom,
            multiplier: 1.0,
            constant: margin))
        addConstraint(NSLayoutConstraint(
            item: loginButton,
            attribute: .width,
            relatedBy: .equal,
            toItem: nil,
            attribute: .notAnAttribute,
            multiplier: 1.0,
            constant: 100))
        
        //6 遮罩图像
        //views 定义 VFL 中控件名称和实际名称的映射关系
        let viewDict = ["maskIconView":maskIconView,
                        "registerButton":registerButton] as [String : Any]
        addConstraints(NSLayoutConstraint.constraints(
                withVisualFormat: "H:|-0-[maskIconView]-0-|",
                options: [],
                metrics: nil,
                views: viewDict))
        addConstraints(NSLayoutConstraint.constraints(
                withVisualFormat: "V:|-0-[maskIconView]-(-20)-[registerButton]",
                options: [],
                metrics: nil,
                views: viewDict))
        
    }
}
