//
//  ZFNavigationController.swift
//  WeiBo
//
//  Created by 左忠飞 on 2016/12/11.
//  Copyright © 2016年 左忠飞. All rights reserved.
//

import UIKit

class ZFNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //隐藏默认的 navigationBar
        navigationBar.isHidden = true
    }

    //重写push 方法,所有的 push 动作都会调用此方法
    //viewController是被push 的控制器,设置他的左侧 annual 作为返回按钮
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        
        //如果不是栈底控制器才需要隐藏,跟控制器不需要处理
        if childViewControllers.count > 0 {
            //隐藏底部的 tabbar
            viewController.hidesBottomBarWhenPushed = true

        
        //判断控制器类型
        if let vc = viewController as? ZFBaseViewController{
            
            var title = "返回"
            
            
            
            //判断控制器的级数,只有一个子控制器的时候,显示栈底控制器的标题
            if childViewControllers.count == 1 {
                //title 显示首页的标题
                title = childViewControllers.first?.title ?? "返回"
            }
            
            vc.navItem.leftBarButtonItem = UIBarButtonItem(title: title, target: self, action: #selector(popToParent),isBack:true)
            
            }
        }
        
        super.pushViewController(viewController, animated: true)
        
    }
    
    @objc fileprivate func popToParent(){
        popViewController(animated: true)
    }

}

