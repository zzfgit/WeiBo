//
//  ZFDemoViewController.swift
//  WeiBo
//
//  Created by 左忠飞 on 2016/12/12.
//  Copyright © 2016年 左忠飞. All rights reserved.
//

import UIKit

class ZFDemoViewController: ZFBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //设置标题
        title = "第\(navigationController?.childViewControllers.count ?? 0)个"
    }

    @objc fileprivate func showNext(){
        let vc = ZFDemoViewController()
        
        navigationController?.pushViewController(vc, animated: true)
        
    }
    

}

extension ZFDemoViewController{
    
    //重写父类方法
    
    override func setupTabelView() {
        super.setupTabelView()
         navItem.rightBarButtonItem = UIBarButtonItem(title: "下一个", fontSize: 16, target: self, action: #selector(showNext))
    }
    
}
