//
//  ZFBaseViewController.swift
//  WeiBo
//
//  Created by 左忠飞 on 2016/12/11.
//  Copyright © 2016年 左忠飞. All rights reserved.
//

import UIKit

//面试题, oc 中不支持多继承,使用协议替代
//swift 的写法更类似与多继承
//class ZFBaseViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

//swift 中可以利用 extension 可以吧函数按照功能分类管理,便于阅读和维护

//注意:
//extension 不能有属性
//extension 中不能重写父类方法!重写父类方法是子类的职责,扩展是对类的扩展

//所有主控制器的基类控制器
class ZFBaseViewController: UIViewController {
    //表格视图,如果用户没有登录,就不创建
    var tableView:UITableView?
    
    var visitorInfoDict:[String:String]?
    
    //自定义导航条
    lazy var navigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: UIScreen.cz_screenWidth(), height: 64))
    
    //自定义导航条目
    lazy var navItem = UINavigationItem()
    
    //刷新控件
    var refreshControl:UIRefreshControl?
    
    //上拉刷新标记
    var isPullup = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        setupUI()

        ZFNetworkManager.shared.userLogin ? loadData() : ()
        
        //注册通知
        NotificationCenter.default.addObserver(self, selector: #selector(loginSuccess), name: NSNotification.Name(rawValue: ZFUserLoginSuccessNotification), object: nil)
    }
    
    deinit {
        //注销通知
        NotificationCenter.default.removeObserver(self)
    }
    
    
    //重写 title 的 didSet
    override var title: String?{
        didSet{
            navItem.title = title
        }
    }
    
    //加载数据 - 具体实现由子类实现
    func loadData(){
        //如果子类不实现加载数据这个方法,默认就关闭刷新控件
        refreshControl?.endRefreshing()
    }
}

//  MARK: - 访客视图的按钮的监听方法
extension ZFBaseViewController {
    
    ///登录成功处理
    @objc func loginSuccess(n:Notification) {
        print("登录成功\(n)")
        
        
        //登录前,左边是注册,右边是登陆,登录完成之后就不需要了
        navItem.leftBarButtonItem = nil
        navItem.rightBarButtonItem = nil
        
        //更新 UI -> 将访客视图替换为表格视图
        //在访问视图的 getter 时,如果 view == nil,回调loadView -> viewdidload
        view = nil
        
        //注销通知 (如果不注销, viewdidload 执行的之后会再次注册通知,造成重复注册)
        NotificationCenter.default.removeObserver(self)
        
    }
    
    @objc fileprivate func userLogin() {
        //发送通知
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:ZFUserShouldLoginNotification), object: nil)
        
    }
    @objc fileprivate func userRegister() {
        print("用户注册")
    }
}

//  MARK: - 设置界面
extension ZFBaseViewController{
     fileprivate func setupUI(){
        view.backgroundColor = UIColor.white
        
        //取消自动缩进  -  如果隐藏了导航栏会缩进20点
        automaticallyAdjustsScrollViewInsets = false
        
        setupNavigationBar()
        ZFNetworkManager.shared.userLogin ? setupTabelView() : setupVisitorView()
    }
    
    //设置访客视图
    private func setupVisitorView(){
        let visitorView = ZFVisitorView(frame: view.bounds)
        
        
        view.insertSubview(visitorView, belowSubview: navigationBar)
        
        //设置访客视图信息
        visitorView.visitorInfo = visitorInfoDict
        
        visitorView.loginButton.addTarget(self, action: #selector(userLogin), for: .touchUpInside)
        visitorView.registerButton.addTarget(self, action: #selector(userRegister), for: .touchUpInside)
        //设置导航条按钮
        navItem.leftBarButtonItem = UIBarButtonItem(title: "注册", style: .plain, target: self, action: #selector(userRegister))
        navItem.rightBarButtonItem = UIBarButtonItem(title: "登录", style: .plain, target: self, action: #selector(userLogin))
    }
    
    //设置 tableView 界面,需要用户登录之后执行
    //子类重写此方法,子类不需要关心用户登录之前的逻辑
    func setupTabelView(){
        tableView = UITableView(frame: view.bounds, style: .plain)
        
        //添加子控件 talbeview, 放到导航栏的下面
        view.insertSubview(tableView!, belowSubview: navigationBar)
        
        //设置数据源和代理,让子类直接实现数据源方法
        tableView?.dataSource = self
        tableView?.delegate = self
        
        //设置内容缩进
        tableView?.contentInset = UIEdgeInsets(top: navigationBar.bounds.height, left: 0, bottom: tabBarController?.tabBar.bounds.height ?? 49, right: 0)
        
        //修改指示器的缩进 - 强行解包是因为肯定有一个 inset
        tableView?.scrollIndicatorInsets = (tableView!.contentInset)
        
        //设置刷新控件
        //1 实例化控件
        refreshControl = UIRefreshControl()
        
        //2 添加到表格视图
        tableView?.addSubview(refreshControl!)
        
        //3 添加坚挺方法
        refreshControl?.addTarget(self, action: #selector(loadData), for: .valueChanged)
    }
    
    //  MARK: - 设置导航条
    fileprivate func setupNavigationBar(){
        //添加导航条
        view.addSubview(navigationBar)
        //将 item设置给 bar
        navigationBar.items = [navItem]
        //设置 navBar 整个背景的渲染颜色
        navigationBar.barTintColor = UIColor.cz_color(withHex: 0xf6f6f6)
        //设置 navBar 的字体颜色
        navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.darkGray];
        //设置系统按钮的文字渲染颜色
        navigationBar.tintColor = UIColor.orange
    }
}

 //  MARK: - UItableViewdelegate,UItableViewDataSource
extension ZFBaseViewController:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    //基类只是准备方法,子类负责具体实现
    //子类的数据源方法不需要 super
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //只是保证没有语法错误
        return UITableViewCell()
    }
    
    //在显示最后一行的时候,做上拉刷新
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //1 判断 indexpath 是不是最后一行
        let row = indexPath.row
        let section = tableView.numberOfSections - 1
        
        if row < 0 || section < 0 {
            return
        }
        
        let count = tableView.numberOfRows(inSection: section)
        
        if row == (count-1) && !isPullup {
            print("上拉刷新")
            isPullup = true
            
            //开始刷新
            loadData()
        }
        
        //rprint("section -- \(section)")
    }
}
