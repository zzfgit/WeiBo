//
//  ZFMainViewController.swift
//  WeiBo
//
//  Created by 左忠飞 on 2016/12/11.
//  Copyright © 2016年 左忠飞. All rights reserved.
//

import UIKit

class ZFMainViewController: UITabBarController {

    //定时器
    fileprivate var timer:Timer?
    
    fileprivate var when = DispatchTime.now()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupChildControllers()
        setupComposeButton()
        setupTimer()
        
        //设置新特性视图
        setupNewfeatureView()
        
        
        //设置代理
        delegate = self
        
        //注册通知
        NotificationCenter.default.addObserver(self, selector: #selector(userLogin), name: NSNotification.Name(rawValue: ZFUserShouldLoginNotification), object: nil)
        
        setupAddtions()
    
    }
    deinit {
        //销毁时钟
        timer?.invalidate()
        
        //注销通知监听
        NotificationCenter.default.removeObserver(self)
        
    }
    
    //这个地方由 iOS9的方法,改成了现在的一个属性
    /**
     使用代码控制设备的方向,好处可以在需要横屏的时候,单独处理
     在当前的控制器以及其自控制器都会遵守这个方向,
     如果播放视频,通常是通过 modal 实现的,不会遵守这个方向设置
     */
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return .portrait
    }
    
    //  MARK: - 登录提醒通知的监听方法
    @objc fileprivate func userLogin(n:Notification) {
        print("用户登录通知\(n)")
        
        //n.object如果有值,说明 token 过期,,提示用户重新登录
        if n.object != nil{
            
            //设置指示器的渐变样式
            SVProgressHUD.setDefaultMaskType(.black)
            
            SVProgressHUD.showInfo(withStatus: "用户登录超时,需要重新登录")
            
            //修改延迟时间
            when = DispatchTime.now()+2
            
        }
        DispatchQueue.main.asyncAfter(deadline: when) {
            
            SVProgressHUD.setDefaultMaskType(.clear)
            
            //展现登录控制器,通常会和 uinavgationController连用,方便返回
            let vc = UINavigationController(rootViewController: ZFOAuthViewController())
            
            self.present(vc, animated: true, completion: nil)
        }
        
        
       
        
    }
    
    
    //@objc允许这个函数在运行时通过 oc 的消息发送机制调用
    //fileprivate 保证方法私有,只能在当前文件中访问
    //  MARK: - 按钮坚挺方法
    @objc fileprivate func composeStatus(){
        print("撰写微博")
        
//        //测试横屏
//        let vc = UIViewController()
//        let nav = UINavigationController(rootViewController: vc)
//        
//        present(nav, animated: true, completion: nil)
        
    }
    
    //撰写按钮
    fileprivate lazy var composeButton:UIButton = UIButton.cz_imageButton("tabbar_compose_icon_add", backgroundImageName: "tabbar_compose_button")

}

//  MARK: -
extension ZFMainViewController:UITabBarControllerDelegate {
    
    
    /// 将要选择的 tabBarItem
    ///
    /// - Parameters:
    ///   - tabBarController: tabBarController
    ///   - viewController: 目标控制器
    /// - Returns: 是否切换到目标控制器
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
         print("将要跳转到\(viewController)")
        
        //获取控制器在数组中的索引
        let index = (childViewControllers as NSArray).index(of: viewController)
        //判断当前索引是首页,同时要切换的也是首页(重复点击首页按钮)
        if selectedIndex == 0 && index == selectedIndex {
            print("重复点击首页")
            //让表格滚动到顶部
            // 获取到控制器
            let nav = childViewControllers[0] as! UINavigationController
            let vc = nav.childViewControllers[0] as! ZFHomeViewController
            
            //滚动到顶部
            vc.tableView?.setContentOffset(CGPoint(x:0,y:-64), animated: true)
            //刷新数据,增加一秒钟的延迟
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1, execute: { 
                vc.loadData()
            })
            
            //清除 tabItem 的 badgeNumber
            vc.tabBarItem.badgeValue = nil
            UIApplication.shared.applicationIconBadgeNumber = 0
            
        }
        
        
        return !viewController.isMember(of: UIViewController.self)
    }
    
}

//  MARK: - 时钟的相关方法
extension ZFMainViewController {
    //定义时钟
    fileprivate func setupTimer() {
        
        //时间间隔建议长一些,因为如果太频繁刷新会被新浪封掉
        timer = Timer.scheduledTimer(timeInterval: 50.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    //时钟触发方法
    
    @objc fileprivate func updateTimer(){
        ZFNetworkManager.shared.unreadCount { (count) in
            
            if !ZFNetworkManager.shared.userLogin{
                return
            }
            
            print("检测到 \(count)条新微薄")
            
            //设置首页 tabBarItem 的 badgeNumber
            self.tabBar.items?[0].badgeValue = count > 0 ? "\(count)" : nil
            
            //设置 app 的 badgeNumber
            UIApplication.shared.applicationIconBadgeNumber = count
            
        }
    }
}


//extension 类似于 oc 中的分类,在 swift 中可以用来切分代码块,可以把相近功能的函数,放在一个 extension 中
//便于代码维护
//和oc中的分类一样,不能定义属性


//  MARK: - 设置界面

extension ZFMainViewController{
    
    
    fileprivate func setupComposeButton(){
        tabBar.addSubview(composeButton)
        
        //设置按钮的宽度
        let count = CGFloat(childViewControllers.count)
        
        //将按钮向内缩进的距离减小1,会让按钮变大一点,盖住容错点,防止点到按钮后边
//        let w = tabBar.bounds.width/count-1
        
        //使用了UITabBarControllerDelegate的 代理方法判断要跳转的目标控制器类型之后再决定跳转不跳转,也可以解决这个问题
        let w = tabBar.bounds.width/count
        
        //insetBy:正数向内缩进,负数向外扩展
        composeButton.frame = tabBar.bounds.insetBy(dx: 2*w, dy: 0)
        
        //按钮监听方法
        composeButton.addTarget(self, action: #selector(composeStatus), for: .touchUpInside)
        
    }
    
    //设置所有子控制器
    func setupChildControllers(){
        
        //0 获取沙盒 JSON 路径
        let docDir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask,true)[0]
        let jsonPath = (docDir as NSString).appendingPathComponent("main.json")
        
        //0.1 加载 data
        var data = NSData(contentsOfFile: jsonPath)
        
        //0.2 判断 data 是否有内容,如果没有说明本地沙盒没有文件
        if data == nil{
            //从 bundle 加载 data
            let path = Bundle.main.path(forResource: "main.json", ofType: nil)
            data = NSData(contentsOfFile: path!)
        }
        
        //从 bundle 加载配置的 JSON
        // 1 路径 2 加载 nsdata 3 反序列化数组
        guard let array = try? JSONSerialization.jsonObject(with: data as! Data, options: []) as? [[String:AnyObject]]
            else {
            return
            }
        
        
        var arrayM = [UIViewController]()
        //遍历数组循环创建控制器数组
        for dict in array!{
            arrayM.append(controller(dict: dict))
        }
        //设置 tabBar 的子控制器
        viewControllers = arrayM
    }
    //使用字典创建一个控制器
    private func controller(dict:[String:AnyObject]) -> UIViewController{
        //1 取得字典内容
        guard let clsName = dict["clsName"] as?String,
            let title = dict["title"] as?String,
            let imageName = dict["imageName"] as?String,
            let cls = NSClassFromString(Bundle.main.namespace + "." + clsName) as? ZFBaseViewController.Type,
            let visitorDict = dict["visitorInfo"] as? [String:String]
            
        
            else {
            return UIViewController()
            }
        //创建试图控制器
        let vc = cls.init()
        vc.title = title
        
        //设置控制器的访客信息字典
        vc.visitorInfoDict = visitorDict
        
        //设置图像
        vc.tabBarItem.image = UIImage(named: "tabbar_"+imageName)
        vc.tabBarItem.selectedImage = UIImage(named: "tabbar_"+imageName+"_selected")?.withRenderingMode(.alwaysOriginal)
        
        //设置 tabbar 标题字体
        vc.tabBarItem.setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.orange], for: .highlighted)
       //修改字体大小,默认是12号,要设置 normal 状态的
        vc.tabBarItem.setTitleTextAttributes([NSFontAttributeName:UIFont.systemFont(ofSize: 10)], for: UIControlState.normal)
        
        //调整 tabbar上的 title的上下偏移量
        vc.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -4)
        
        //实例化导航控制器的时候,会调用 push 方法将 rootVC 压栈
        let nav = ZFNavigationController(rootViewController: vc)
        return nav
    }
    
    
}


//  MARK: - 设置应用程序额外信息
extension ZFMainViewController {
    
    fileprivate func setupAddtions() {
        //设置 svprogresshud 最小解除时间
        SVProgressHUD.setMinimumDismissTimeInterval(1)
    }
}

//  MARK: - 新特性视图处理
extension ZFMainViewController {
    fileprivate func setupNewfeatureView() {
        
        //0判断是否登录
        //如果没登录就什么都不做
        if !ZFNetworkManager.shared.userLogin {
            return
        }
        
        //1检查版是否更新
        let v = isNewVersion ? ZFNewFeture.newFeature() : ZFWelcomeView.welcome()
        //2如果更新,显示新特性,否则显示欢迎
        //3添加视图
        view.addSubview(v)
        
    }
    
    /**
      版本号
      每次升级应用程序,版本号都需要增加
      组成:祝版本号,次版本号,修订版本号
      祝版本号:意味着大的修改,使用者也需要做大的适应
      次版本号:意味着小的修改,某些函数和方法的使用或者参数有变化
      修订版本号:程序框架或者 bug 的修订,不会对使用者造成任何的影响
     */
    
    ///extension 中可以有计算型属性,不会占用存储空间 
    ///构造函数:给属性分配空间
    fileprivate var isNewVersion:Bool {
        
        //1去当前的版本号
        //print(Bundle.main.infoDictionary)
        let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        print(currentVersion)
         //2取保存在'Document'目录中之前的版本号
        let path = ("version" as NSString).cz_appendDocumentDir()
        let sandboxVersion = (try? String(contentsOfFile: path!)) ?? ""
        
        print(sandboxVersion)
        //3将当前版本号保存在沙盒中
        _=try? currentVersion.write(toFile: path!, atomically: true, encoding: .utf8)
        
        //4返回两个版本号是否一致
         return currentVersion != sandboxVersion
//        return currentVersion == sandboxVersion
    }
}











