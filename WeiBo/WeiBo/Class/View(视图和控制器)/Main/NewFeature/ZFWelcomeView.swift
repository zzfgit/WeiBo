//
//  ZFWelcomeView.swift
//  WeiBo
//
//  Created by 左忠飞 on 2016/12/31.
//  Copyright © 2016年 左忠飞. All rights reserved.
//

import UIKit

//欢迎视图
class ZFWelcomeView: UIView {

    @IBOutlet weak var iconView: UIImageView!
    
    @IBOutlet weak var tipLabel: UILabel!
    
    @IBOutlet weak var bottomCons: NSLayoutConstraint!
    
    
    class func welcome() ->ZFWelcomeView {
        let nib = UINib(nibName: "ZFWelcomeView", bundle: nil)
        let v = nib.instantiate(withOwner: nil, options: nil)[0] as! ZFWelcomeView
        
        //从 xib 加载的视图默认是600*600
        v.frame = UIScreen.main.bounds
        
        return v
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //在这个方法里只是刚从 xib 的二进制文件将视图加载完成
        //还没有和代码连线建立起关系,所以开发中,不要在这个方法中处理 UI
    }
    
    override func awakeFromNib() {
        //1 url
        guard let urlString = ZFNetworkManager.shared.userAccount.avatar_large,
            let url = URL.init(string: urlString) else {
            return
        }
        
        //2 设置头像,如果网络图像没有下载完,先显示展位图像
        //如果不指定展位图像,之前设置的图像会被清空
        iconView.sd_setImage(with: url, placeholderImage: UIImage(named: "avatar_default_big"))
        
    }
    
    ///视图被添加到 Windows, 表示视图已经显示
    override func didMoveToWindow() {
        super.didMoveToWindow()
        
        //视图是使用自动布局来设置的,当视图被添加到窗口上时,会根据父视图的大小去计算约束值,然后更新位置
        //如果控件们的 frame 还没计算好,所有的约束会一起动画
        //layoutIfneed会直接按照当前约束更新控件的位置
        //执行之后,控件的位置,就是 xib 中布局的位置
        //***首先更新一下已经设置的约束
        self.layoutIfNeeded()
        
        
        bottomCons.constant = bounds.size.height - 200
        UIView.animate(withDuration: 1.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
            
            //如果约束更改了,在动画中再次更新约束
            self.layoutIfNeeded()
        }) { (_) in
            UIView.animate(withDuration: 1.0, animations: { 
                self.tipLabel.alpha = 1
            }, completion: { (_) in
                self.removeFromSuperview()
            })
        }
        
        
        
        
    }
  
}
