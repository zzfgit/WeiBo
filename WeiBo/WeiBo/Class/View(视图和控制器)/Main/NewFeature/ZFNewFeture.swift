//
//  ZFNewFeture.swift
//  WeiBo
//
//  Created by 左忠飞 on 2016/12/31.
//  Copyright © 2016年 左忠飞. All rights reserved.
//

import UIKit

///新特性视图
class ZFNewFeture: UIView {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var enterBtn: UIButton!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    //进入微博
    @IBAction func enterWeibo(_ sender: Any) {
        removeFromSuperview()
    }
    
    class func newFeature() ->ZFNewFeture {
        let nib = UINib(nibName: "ZFNewFeture", bundle: nil)
        let v = nib.instantiate(withOwner: nil, options: nil)[0] as! ZFNewFeture
        
        //从 xib 加载的视图默认是600*600
        v.frame = UIScreen.main.bounds
        
        return v
    }
    override func awakeFromNib() {
        //如果使用自动布局设置的界面,从 XIB 中加载的视图默认大小是600*600的
        //添加4个图像视图
        let count = 4
        let rect = UIScreen.main.bounds
        for i in 0..<count{
            let imageName = "new_feature_\(i+1)"
            let imageView = UIImageView(image: UIImage.init(named: imageName))
            //设置大小
            imageView.frame = rect.offsetBy(dx: CGFloat(i)*rect.width, dy: 0)
            
            scrollView.addSubview(imageView)
            
        }
        //指定 scrollView 的属性
        scrollView.contentSize = CGSize(width: CGFloat(count+1)*rect.width, height: rect.height)
        scrollView.bounces = false
        scrollView.isPagingEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        
        scrollView.delegate = self
        
        
        enterBtn.isHidden = true
        
            }
}


extension ZFNewFeture:UIScrollViewDelegate{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //滚动到最后一屏,让视图删除
        let page = Int(scrollView.contentOffset.x / scrollView.bounds.width)
        //判断是否最后一页
        if page == scrollView.subviews.count {
            
            print("欢迎欢迎")
            
            removeFromSuperview()
            
        }
        //如果是倒数第二页显示按钮
        enterBtn.isHidden = (page != scrollView.subviews.count - 1)
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        //滚动的时候隐藏按钮
        enterBtn.isHidden = true
        
        //计算当前的偏移量
        let page = Int(scrollView.contentOffset.x / scrollView.bounds.width + 0.5)
        //设置分页控件
        pageControl.currentPage = page
        //分页控件的隐藏
        pageControl.isHidden = (page == scrollView.subviews.count)
    }
}
