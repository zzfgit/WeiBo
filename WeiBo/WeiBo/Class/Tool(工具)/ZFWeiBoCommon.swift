//
//  ZFWeiBoCommon.swift
//  WeiBo
//
//  Created by 左忠飞 on 2016/12/27.
//  Copyright © 2016年 左忠飞. All rights reserved.
//

import Foundation

//  MARK: - 定义全局通知

//用户需要登录通知
let ZFUserShouldLoginNotification = "ZFUserShouldLoginNotification"

//用户登录成功通知
let ZFUserLoginSuccessNotification = "ZFUserLoginSuccessNotification"


///应用程序 id
let ZFAppKey = "2544063768"

///应用程序加密信息
let ZFAppSecret = "c4295f0cf7cce9b6ea3bf745b10d540c"

///回调地址(登录完成跳转的 url)
let ZFRedirectURI = "http://www.jianshu.com"

