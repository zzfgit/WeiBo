
//
//  ZFNetworking+Extension.swift
//  WeiBo
//
//  Created by 左忠飞 on 2016/12/23.
//  Copyright © 2016年 左忠飞. All rights reserved.
//

import Foundation

//  MARK: - 封装新浪微博的网络请求方法
extension ZFNetworkManager{
    
    func statusList(since_id:Int64 = 0,max_id:Int64 = 0,completion: @escaping (_ list:[[String:AnyObject]]?,_ isSuccess:Bool)->()) {
        let urlString = "https://api.weibo.com/2/statuses/home_timeline.json"
        
        let params = ["since_id":"\(since_id)","max_id":"\(max_id > 0 ? max_id - 1 : 0 )"]
        
        tokenRequest(URLString: urlString, parameters: params as [String : AnyObject]?) { (json, isSuccess) in

            //从 JSON 中局偶去 status 字典数组
            //如果 as? 失败, result = nil
            let result = json?["statuses"] as? [[String: AnyObject]]
            completion(result, isSuccess)
        }
    }
    
    //返回微博的未读数量
    func unreadCount(completion:@escaping (_ count:Int)->()){
        let urlString = "https://rm.api.weibo.com/2/remind/unread_count.json"
        
        guard let uid = userAccount.uid else {
            return
        }
        
        let params = ["uid":uid]
        
        tokenRequest(URLString: urlString, parameters: params as [String : AnyObject]?) { (json, isSuccess)
            in
            
            let dict = json as? [String:AnyObject]
            let count = dict?["status"] as? Int
            
            completion(count ?? 0)
        }
        
    }
    
}


//  MARK: - 加载用户信息
extension ZFNetworkManager {
    //加载用户信息
    func loadUserInfo(completion:@escaping (_ dict:[String:AnyObject])->()) {
        guard let uid = userAccount.uid else {
            return
        }
        let urlString = "https://api.weibo.com/2/users/show.json"
        let params = ["uid":uid]
        
        //发起网络请求
        tokenRequest(URLString: urlString, parameters: params as [String : AnyObject]?) { (json, isSuccess) in
//            print("***************\(json!)")
            //完成回调
            completion(json as! [String : AnyObject])
        }
    }
}

//  MARK: - OAuth相关方法
extension ZFNetworkManager {
    
    
    /// 加载 accesstoken
    ///
    /// - Parameters:
    ///   - code: 授权码
    ///   - completion: 完成回调(是否成功)
    func loadAccessToken(code:String,completion:@escaping (_ isSuccess:Bool)->()) {
        let urlString = "https://api.weibo.com/oauth2/access_token"
        
        let params = ["client_id": ZFAppKey,
                      "client_secret": ZFAppSecret,
                      "grant_type": "authorization_code",
                      "code": code,
                      "redirect_uri": ZFRedirectURI,
                      ]
        
        //发起网络请求
        request(method: .POST, URLString: urlString, parameters: params as [String : AnyObject]) { (json, isSuccess) in
            //print(json)
            
            //如果请求失败,对用户账户数据不会有任何影响
            self.userAccount.yy_modelSet(with: (json as? [String : AnyObject]) ?? [:])
            
            //print(self.userAccount)
            
            //加载当前用户信息
            self.loadUserInfo(completion: { (dict) in
                
                //使用用户信息字典设置用户账户信息(昵称和头像)
                self.userAccount.yy_modelSet(with: dict)
                
                //保存模型
                self.userAccount.saveAccount()
                

                print(self.userAccount)
            })
            
            //用户信息加载完成,再完成回调
            completion(isSuccess)
        }
    }
}












