//
//  ZFNetworkManager.swift
//  WeiBo
//
//  Created by 左忠飞 on 2016/12/23.
//  Copyright © 2016年 左忠飞. All rights reserved.
//

import UIKit

//Swift中的枚举支持任意数据类型
enum ZFHTTPMethod {
    case GET
    case POST
}

class ZFNetworkManager: AFHTTPSessionManager {

    //定义网络管理对象的单例对象,用 static 修饰,存放在静态区(常量区),
    //在第一次访问时执行闭包,并且将结果保存在 shared 常量中
    static let shared = { () -> ZFNetworkManager in 
        
        //实例化对象
        let instance = ZFNetworkManager()
        
        //设置响应的反序列化支持的数据类型
        instance.responseSerializer.acceptableContentTypes?.insert("text/plain")
        
        //返回对象
        return instance
        
    }()
    
//    ///访问令牌:所有的网络请求,都需要此令牌
//    ///为了保护用户安全, token 是有时限的,默认用户是三天
//    var accessToken:String? = "2.00p68gOEaMdKmC70541609aezWOFcD"
//    
//    //用户微博 id
//    var uid:String? = "3881515197"
//    
    
    //用户账户的懒加载属性
    lazy var userAccount = ZFUserAccount()
    
    
    //用户登录标记
    var userLogin:Bool {
        return  userAccount.access_token != nil
    }
    
    func tokenRequest(method:ZFHTTPMethod = .GET, URLString: String, parameters:[String:AnyObject]?,completion:@escaping (_ json:AnyObject?,_ isSuccess:Bool)->()){
        
        //处理 token 字典
        //处理 token, 判断 token 是否为 nil
        guard let token = userAccount.access_token else {
            
            //发送通知,提示用户登录
            print("没有 token 值,需要登录")
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: ZFUserShouldLoginNotification), object: "bad token")
            
            completion(nil, false)
            return
        }
        
        var parameters = parameters
        
        if parameters == nil {
            //实例化字典
            parameters = [String:AnyObject]();
            
        }
        
        //设置参数字典
        parameters!["access_token"] = token as AnyObject?
        //调用 request 发起真正的网络请求方法
        request(URLString: URLString, parameters: parameters!, completion: completion)
        
    }
    
    //使用一个函数封装 apn 的 GET 和 POST 请求
    
    /// 封装 afn 的 GET,POST 请求方法
    ///
    /// - Parameters:
    ///   - method: 请求类型
    ///   - URLString:  URL
    ///   - parameters: 参数字典
    ///   - completion: 完成回调[JSON(字典/数组),是否成功]
    func request(method:ZFHTTPMethod = .GET, URLString: String, parameters:[String:AnyObject],completion:@escaping (_ json:AnyObject?,_ isSuccess:Bool)->()) {
        
        let success = { (task:URLSessionDataTask,json:Any?)->() in
            completion(json as AnyObject?, true)
        }
        let failure = { (task:URLSessionDataTask?,error:Error)->() in
            
            //针对403处理用户 token 过期
            if (task?.response as? HTTPURLResponse)?.statusCode == 403 {
                print("token 过期了")
                // 发送通知,提示用户登录(本方法不知道被谁调用,谁接收到通知谁处理,)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: ZFUserShouldLoginNotification), object: nil)
            }
            
            //错误信息通畅比较吓人,例如编号: xxxx, 错误原因一大堆英文,所以不要返回去让用户知道
            print("网络请求错误\(error)")
            
            completion(nil, false)
        }
        
        if method == .GET {
            get(URLString, parameters: parameters, progress: nil, success: success, failure: failure)
        }else{
            post(URLString, parameters: parameters, progress: nil, success: success, failure: failure)
        }
        
    }
    
    
}
