//
//  UIarButtonItem+Extensions.swift
//  WeiBo
//
//  Created by 左忠飞 on 2016/12/12.
//  Copyright © 2016年 左忠飞. All rights reserved.
//

import UIKit


extension UIBarButtonItem{
    
    
    /// 创建UIBarButtonItem
    ///
    /// - Parameters:
    ///   - title: title,标题
    ///   - fontSize: 文字大小,默认16
    ///   - target: target
    ///   - action: action
    ///   - action: isBack是否是返回按钮,如果是加上箭头
    convenience init(title:String,fontSize:CGFloat = 16,target:AnyObject?,action:Selector,isBack:Bool = false){
        
        let btn:UIButton = UIButton.cz_textButton(title, fontSize: fontSize, normalColor: UIColor.darkGray, highlightedColor: UIColor.orange)
        
        if isBack {
            
            let imageName = "navigationbar_back_withtext"
            btn.setImage(UIImage(named:imageName), for: UIControlState(rawValue: 0))
            btn.setImage(UIImage(named:imageName + "_highlighted"), for:.highlighted)
            
            btn.sizeToFit()
        }
        
        btn.addTarget(target, action:action, for: .touchUpInside)
        
        //self.init 实例化UIBarButtonItem
        self.init(customView:btn)
    }

}

