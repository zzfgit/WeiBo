//
//  ZFStatus.swift
//  WeiBo
//
//  Created by 左忠飞 on 2016/12/23.
//  Copyright © 2016年 左忠飞. All rights reserved.
//

import UIKit


///微博数据模型
class ZFStatus: NSObject {
    
    //int类型,在64为的机器是64位的,在32为的机器就是32位的
    //如果不写 Int64,在 iPhone5以及之前的老设备都无法正常运行
    var id:Int64 = 0
    
    //微博的信息内容
    var text:String?
    
    
    //重写 description 的计算型属性
    override var description: String {
         return yy_modelDescription()
    }
    
}
